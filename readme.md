This project was created using Vite with React

To run the project do the following:
- Clone the repository
- Run `npm install` or `yarn` to install dependencies
- Run `npm run dev` or `yarn dev` to start running the server
- Open `localhost:3000` in your browser