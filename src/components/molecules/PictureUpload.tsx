import React, { useState } from 'react'
import Avatar from '../atoms/Avatar'
import { Edit } from 'react-feather'
import { Controller } from 'react-hook-form';
import { toast } from 'react-toastify';

const PictureUpload = ({ register, process, rules, name, ...rest }: any) => {
  const [image, setImage] = useState<string | ArrayBuffer | null>(null);

  const validateFormat = (file: File) => {
    const fileFormat = ['image/png', 'image/jpg', 'image/jpeg', 'image/gif'].includes(file.type);
    return fileFormat;
  }

  const handleOnChange = (event: any) => {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      setImage(reader.result);
      process(name, reader.result);
    });
    if (file && validateFormat(file)) {
      reader.readAsDataURL(file);
    } else {
      toast.error(`Wrong file format`);
    }
  }
  return (
    <div className="relative">
      <Avatar src={image} />
      <input type="file" accept="image/jpg, image/jpeg, image/png, image/gif" id={name} hidden {...register(name, {onChange: handleOnChange})} {...rest} />
      <label htmlFor={name} className="cursor-pointer absolute bottom-0 right-0 w-10 h-10 text-gray-800 active:text-primary-active rounded-full bg-primary-base hover:bg-primary-hover focus:bg-primary-hover flex justify-center items-center">
        <Edit />
      </label>
    </div>
  );
}

export default PictureUpload;
