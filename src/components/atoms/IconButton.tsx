import React from 'react'

const IconButton = ({ children }: any) => {
  return (
    <button type="button" className="w-10 h-10 text-gray-800 active:text-primary-active rounded-full bg-primary-base hover:bg-primary-hover focus:bg-primary-hover flex justify-center items-center">
      {children}
    </button>
  )
}

export default IconButton
