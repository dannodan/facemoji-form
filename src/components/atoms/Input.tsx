import React from 'react';
import { FieldError } from 'react-hook-form';

interface InputProps extends Partial<HTMLInputElement> {
  register?: any;
  process?: any;
  rules?: object;
  errors?: FieldError;
  name: string;
}

const Input = ({ register, process, rules, name, errors, ...rest }: InputProps) => {
  errors && console.log(errors);
  return (
    <>
      <input
        {...register(name, rules)}
        {...rest}
        className={`w-full mt-2 py-2 px-3 text-gray-800 rounded-md bg-gray-200 hover:bg-gray-300 focus:bg-white focus:outline-none focus:ring-1 ${!errors ? 'focus:ring-gray-400 mb-2' : 'ring-1 ring-red-500 text-red-500'}`}
      />
      {errors && <div className="mb-2 text-red-500 self-start text-xs">{errors.message}</div>}
    </>
  )
}

export default Input;
