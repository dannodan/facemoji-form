import React from 'react'

interface TextfieldProps extends Partial<HTMLInputElement> {
  register?: any;
  process?: any;
  rules?: object;
  errors?: object;
  name: string;
}

const Textfield = ({ register, process, rules, name, errors, ...rest }: TextfieldProps) => {
  return (
    <textarea
      {...register(name, rules)}
      {...rest}
      className={`w-full resize-none my-2 py-2 px-3 text-gray-800 rounded-md bg-gray-200 hover:bg-gray-300 focus:bg-white focus:outline-none focus:ring-1 ${!errors ? 'focus:ring-gray-400' : 'ring-1 ring-red-500 text-red-500'}`}
    />
  )
}

export default Textfield
