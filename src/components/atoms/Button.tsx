import React from 'react'

const Button = ({ children, type }: any) => {
  return (
    <button type={type} className="py-2 px-4 text-gray-800 active:text-primary-active rounded-md bg-primary-base hover:bg-primary-hover focus:bg-primary-hover">
      {children}
    </button>
  )
}

export default Button
