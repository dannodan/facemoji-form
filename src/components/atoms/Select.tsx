import { ErrorMessage } from '@hookform/error-message'
import React from 'react'
import { FieldError } from 'react-hook-form'

interface SelectProps extends Partial<HTMLSelectElement> {
  register?: any;
  process?: any;
  rules?: object;
  errors?: FieldError;
  optionList: {name: string, value: string}[];
  name: string;
}

const Select = ({ register, process, rules, name, errors, optionList, ...rest }: SelectProps) => {
  return (
    <>
      <select
        {...register(name, rules)}
        {...rest}
        className={`transition-width self-start duration-500 ease-in-out min-w-max w-10 hover:w-full focus:w-full mt-2 py-2 px-3 text-gray-800 rounded-md bg-white hover:ring-1 hover:ring-gray-400 focus:bg-white focus:outline-none focus:ring-1 ${!errors ? 'focus:ring-gray-500 mb-2' : 'ring-1 ring-red-500 text-red-500 hover:ring-red-500'}`}
      >
        <option value="" disabled selected hidden>Category *</option>
        {optionList.map(entry => (
          <option key={entry.value} value={entry.value} className="text-gray-800">{entry.name}</option>
        ))}
      </select>
      {errors && <div className="mb-2 text-red-500 self-start text-xs">{errors.message}</div>}
    </>
  )
}

export default Select
