import React from 'react'

const Avatar = ({ src }: any) => {
  return (
    <div className="w-32 h-32 bg-gray-300 rounded-full bg-contain" style={{ backgroundImage: `url(${src})`}}></div>
  )
}

export default Avatar
