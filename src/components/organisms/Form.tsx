import React from 'react'
import { useForm } from 'react-hook-form'
import { toast } from 'react-toastify';

const Form = ({ defaultValues, children, className, onSubmit }: any) => {
  const methods = useForm({ defaultValues });
  const { handleSubmit, formState: { errors } } = methods;
  const onError = (data: any) => {
    console.log('Data:', data);
    toast.error(`There was an error`);
  }
  return (
    <form onSubmit={handleSubmit(onSubmit, onError)} className={className}>
      {React.Children.map(children, child => {
        return child.props.name
          ? React.createElement(child.type, {
            ...{
              ...child.props,
              register: methods.register,
              process: methods.setValue,
              rules: child.props.rules,
              errors: errors[child.props.name],
              key: child.props.name,
            },
          }) : child
        })}
    </form>
  )
}

export default Form
