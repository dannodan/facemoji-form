import './App.css'
import Input from './components/atoms/Input'
import Form from './components/organisms/Form'
import Button from './components/atoms/Button'
import Textfield from './components/atoms/Textfield'
import PictureUpload from './components/molecules/PictureUpload'
import Select from './components/atoms/Select'
import axios from 'axios'
import { toast } from 'react-toastify'

function App() {
  const defaultValues = {
    name: null,
    category: null,
    description: null,
    website: null,
    icon: null,
  }

  const options = [
    {value: 'economy', name: 'Economy'},
    {value: 'entertainment', name: 'Entertainment'},
    {value: 'games', name: 'Games'},
    {value: 'health', name: 'Health'},
    {value: 'productivity', name: 'Productivity'},
    {value: 'news', name: 'News'},

  ]

  const processData = (data: any) => {
    for (const [key, value] of Object.entries(data)) {
      if (!value) {
        data[key] = null;
      }
    };
    return data;
  }

  const onSubmit = (data: any) => {
    const processedData = processData(data)
    axios.post('https://reqres.in/api/applications', {...processedData}).then(response => {
      console.log(response);
      toast.success('App created successfully');
    }).catch(response => {
      toast.error('There was an error in the request');
      toast.error("This will always happen when adding an image because this endpoint doesn't allow big payloads");
    })
  }
  return (
    <div className="App mx-4">
      <div className="font-bold text-xl text-center my-4">Creating an App</div>
      <Form className="flex flex-col justify-center items-center max-w-sm mx-auto" defaultValues={defaultValues} onSubmit={onSubmit}>
        <PictureUpload name="icon" />
        <div className="text-gray-600 text-xs mb-3">Image must be .jpeg, .png or .gif</div>
        <Input name="name" placeholder="Display name *" rules={{required: "The App's display name is required"}} />
        <Select name="category" rules={{required: "A category needs to be selected"}} optionList={options} />
        <Textfield name="description" placeholder="Description" />
        <Input name="website" placeholder="Company website" />
        <div className="mt-4">
          <Button type="submit">Create App</Button>
        </div>
      </Form>
    </div>
  );
}

export default App
