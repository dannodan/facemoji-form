const plugin = require('tailwindcss/plugin');

module.exports = {
  mode: 'jit',
  purge: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      transitionProperty: {
        'width': 'width',
      },
      colors: {
        primary: { 
          'base': '#00FFCC',
          'hover': '#00F2C2',
          'active': '#00B38F',
        },
      }
    },
  },
  variants: {
    extend: {
      ringColor: ['invalid'],
      textColor: ['invalid']
    },
  },
  plugins: [
    plugin(function({ addVariant, e }) {
      addVariant('invalid', ({ modifySelectors, separator }) => {
        modifySelectors(({ className }) => {
          return `.${e(`invalid${separator}${className}`)}:invalid`
        })
      })
    })
  ],
}
